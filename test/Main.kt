package com.todolist

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.todolist.models.SuccessResponse
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.server.testing.*
import com.todolist.models.TodoEntry
import kotlin.test.*

class ApplicationTest {
    private val mapper = com.fasterxml.jackson.module.kotlin.jacksonObjectMapper()

    @Test
    fun testReadEntryWithWrongId() = withTestApplication(Application::module) {
        with(handleRequest(HttpMethod.Get, "/todoEntries/UNKNOWN")) {
            assertEquals(io.ktor.http.HttpStatusCode.NotFound, response.status())
        }
    }

    @Test
    fun testReadEntry() = withTestApplication(Application::module) {
        var newEntryId: Int = -1

        with(handleRequest(HttpMethod.Post, "/todoEntries", {
            setBody(mapper.writeValueAsString(mapOf("value" to "test")))
        })) {
            val result = mapper.readValue<SuccessResponse<TodoEntry>>(response.content!!)
            assertTrue { result.data.id > 1 }
            assertEquals(result.data.value, "test")

            newEntryId = result.data.id
        }

        with(handleRequest(HttpMethod.Get, "/todoEntries/$newEntryId")) {
            val result = mapper.readValue<SuccessResponse<TodoEntry>>(response.content!!)

            assertEquals(io.ktor.http.HttpStatusCode.OK, response.status())
            assertEquals(result.data.id, newEntryId)
            assertEquals(result.data.value, "test")
            assertEquals(result.data.done, false)
        }
    }

    @Test
    fun testUpdateEntry() = withTestApplication(Application::module) {
        var newEntryId: Int = -1

        with(handleRequest(HttpMethod.Post, "/todoEntries", {
            setBody(mapper.writeValueAsString(mapOf("value" to "test")))
        })) {
            val result = mapper.readValue<SuccessResponse<TodoEntry>>(response.content!!)
            newEntryId = result.data.id
        }

        with(handleRequest(HttpMethod.Post, "/todoEntries/$newEntryId", {
            setBody(mapper.writeValueAsString(mapOf("value" to "test3", "done" to true)))
        })) {
            assertEquals(io.ktor.http.HttpStatusCode.OK, response.status())
        }

        with(handleRequest(HttpMethod.Get, "/todoEntries/$newEntryId")) {
            val result = mapper.readValue<SuccessResponse<TodoEntry>>(response.content!!)
            assertEquals(result.data.value, "test3")
            assertEquals(result.data.done, true)
        }
    }

    @Test
    fun testGetAll() = withTestApplication(Application::module) {
        with(handleRequest(HttpMethod.Post, "/todoEntries", {
            setBody(mapper.writeValueAsString(mapOf("value" to "test")))
        })) {
            assertEquals(io.ktor.http.HttpStatusCode.OK, response.status())
        }

        with(handleRequest(HttpMethod.Get, "/todoEntries?filter=all")) {
            val result: SuccessResponse<List<TodoEntry>> = mapper.readValue(response.content!!)
            assertTrue { result.data.count() > 0}
        }
    }

    @Test
    fun testGetNotDone() = withTestApplication(Application::module) {
        var newEntryId: Int = -1

        with(handleRequest(HttpMethod.Post, "/todoEntries", {
            setBody(mapper.writeValueAsString(mapOf("value" to "test")))
        })) {
            val result: SuccessResponse<TodoEntry> = mapper.readValue(response.content!!)
            newEntryId = result.data.id
        }

        with(handleRequest(HttpMethod.Get, "/todoEntries?filter=not_done")) {
            val result: SuccessResponse<List<TodoEntry>> = mapper.readValue(response.content!!)
            assertEquals(result.data.filter { it.id == newEntryId }.count(), 1)
        }
    }

    @Test
    fun testGetDone() = withTestApplication(Application::module) {
        var newEntryId: Int = -1

        with(handleRequest(HttpMethod.Post, "/todoEntries", {
            setBody(mapper.writeValueAsString(mapOf("value" to "test", "done" to true)))
        })) {
            val result = mapper.readValue<SuccessResponse<TodoEntry>>(response.content!!)
            newEntryId = result.data.id
        }

        with(handleRequest(HttpMethod.Get, "/todoEntries?filter=done")) {
            val result: SuccessResponse<List<TodoEntry>> = mapper.readValue(response.content!!)
            assertEquals(result.data.filter { it.id == newEntryId }.count(), 1)
        }
    }

    @Test
    fun testDelete() = withTestApplication(Application::module) {
        var newEntryId: Int = -1

        with(handleRequest(HttpMethod.Post, "/todoEntries", {
            setBody(mapper.writeValueAsString(mapOf("value" to "test")))
        })) {
            val result = mapper.readValue<SuccessResponse<TodoEntry>>(response.content!!)
            newEntryId = result.data.id
        }

        with(handleRequest(HttpMethod.Delete, "/todoEntries/$newEntryId")) {
            assertEquals(io.ktor.http.HttpStatusCode.OK, response.status())
        }

        with(handleRequest(HttpMethod.Get, "/todoEntries/$newEntryId")) {
            assertEquals(io.ktor.http.HttpStatusCode.NotFound, response.status())
        }
    }
}