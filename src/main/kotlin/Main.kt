package com.todolist

import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.*
import io.ktor.jackson.jackson
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.http.HttpMethod
import com.todolist.models.DatabaseEngine
import com.todolist.models.ErrorResponse
import com.todolist.web.NotFoundEntryException
import com.todolist.web.ServerException
import com.todolist.web.UnknownFilterException
import com.todolist.web.todoEntries
import io.ktor.features.*
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond

fun Application.module() {
    DatabaseEngine.init()

    install(DefaultHeaders)
    install(CallLogging)
    install(CORS) {
        anyHost()
        method(HttpMethod.Get)
        method(HttpMethod.Post)
        method(HttpMethod.Options)
        method(HttpMethod.Delete)
    }
    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT) // Pretty Prints the JSON
        }
    }

    install(StatusPages) {
        exception<ServerException> {
            call.respond(HttpStatusCode.BadRequest, ErrorResponse(it.description))
        }

        exception<NotFoundEntryException> {
            call.respond(HttpStatusCode.NotFound, ErrorResponse(it.description))
        }
    }

    routing {
        todoEntries()
    }
}

fun main(args: Array<String>) {
    val portVal = System.getenv("PORT") ?: "8090"
    val port = portVal.toInt()
    println("Server listens on http://localhost:$port")
    embeddedServer(Netty, port, watchPaths = listOf("TodoList"), module = Application::module).start(wait = true)
}