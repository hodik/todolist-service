package com.todolist.models

import org.jetbrains.exposed.sql.*

object TodoEntryService {
    suspend fun getById(id: Int): TodoEntry? = DatabaseEngine.dbQuery {
        TodoEntries
                .select { TodoEntries.id eq id }
                .mapNotNull { toTodoEntry(it) }
                .singleOrNull()
    }

    suspend fun updateById(id: Int, changes: NewTodoEntryRequest) = DatabaseEngine.dbQuery {
        TodoEntries.update({ TodoEntries.id eq id }) {
            it[TodoEntries.value] = changes.value
            it[TodoEntries.done] = changes.done
        }
    }

    suspend fun deleteById(id: Int) = DatabaseEngine.dbQuery {
        TodoEntries.deleteWhere { TodoEntries.id eq id }
    }

    suspend fun create(request: NewTodoEntryRequest): Int? = DatabaseEngine.dbQuery {
        TodoEntries.insert{
            it[TodoEntries.value] = request.value
            it[TodoEntries.done] = request.done
        } get TodoEntries.id
    }

    private fun toTodoEntry(row: ResultRow): TodoEntry {
        return TodoEntry(
            id = row[TodoEntries.id],
            value = row[TodoEntries.value],
            done = row[TodoEntries.done]
        )
    }

    suspend fun getAll(): List<TodoEntry> = DatabaseEngine.dbQuery {
        TodoEntries.selectAll().map { toTodoEntry(it) }
    }

    suspend fun filterEntries(done: Boolean): List<TodoEntry> = DatabaseEngine.dbQuery {
        TodoEntries.select { TodoEntries.done eq done }.map {
            toTodoEntry(it)
        }
    }
}