package com.todolist.models

abstract class BaseResponse(val error: Boolean);
data class ErrorResponse(val description: String): BaseResponse(error=true);
data class SuccessResponse<T>(val data: T): BaseResponse(error=false)