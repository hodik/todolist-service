package com.todolist.models

import org.jetbrains.exposed.sql.Table

data class TodoEntry(var id: Int, val value: String, val done: Boolean? = false)
data class NewTodoEntryRequest(val value: String, val done: Boolean)

object TodoEntries: Table() {
    val id = integer("id").autoIncrement().primaryKey()
    val value = varchar("value", length = 128)
    val done = bool("done")
}