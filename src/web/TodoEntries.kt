package com.todolist.web

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.request.receive
import io.ktor.routing.*
import com.todolist.models.*

enum class FilterType {
    ALL, DONE, NOT_DONE
}

open class ServerException(val description: String): RuntimeException()
class NotFoundEntryException(): ServerException(description = "Unknown entry")
class UnknownFilterException(): ServerException(description = "Unknown filter")

fun Route.todoEntries() {
    route("/todoEntries") {
        get("/{id}") {
            val id: Int = call.parameters["id"]?.toIntOrNull() ?: -1
            val entry: TodoEntry? = TodoEntryService.getById(id)

            if(entry != null) {
                call.respond(SuccessResponse(entry))
            } else {
                throw NotFoundEntryException()
            }
        }

        post("/{id}") {
            val request = call.receive<NewTodoEntryRequest>()
            val id: Int = call.parameters["id"]?.toIntOrNull() ?: -1

            TodoEntryService.updateById(id, request)

            call.respond(SuccessResponse("Updated"))
        }

        delete("/{id}") {
            val id: Int = call.parameters["id"]?.toIntOrNull() ?: -1
            TodoEntryService.deleteById(id)
            call.respond(SuccessResponse("Removed"))
        }

        get("/") {
            val filterTypeValue = call.request.queryParameters.get("filter")!!
            var filterType: FilterType
            try {
                filterType = FilterType.valueOf(filterTypeValue.toUpperCase())
            } catch (error: IllegalArgumentException) {
                throw UnknownFilterException()
            }

            val entries: List<TodoEntry> = if(filterType == FilterType.ALL) {
                TodoEntryService.getAll()
            } else {
                TodoEntryService.filterEntries(filterType == FilterType.DONE)
            }

            call.respond(SuccessResponse(entries))
        }

        post("/") {
            val request = call.receive<NewTodoEntryRequest>()
            val id = TodoEntryService.create(request)
            if(id == null) {
                call.respond(HttpStatusCode.BadRequest, ErrorResponse("Entry not created"))
            } else {
                val entry = TodoEntry(id = id, value = request.value, done = false)
                call.respond(SuccessResponse(entry))
            }
        }
    }
}